const fs = require('fs');
const path = require('path');
const {sequelize, Employee, Department, Statement, Donation} = require('./src/models');

const generateRatesMap = (ratesData) => {
    const rateRegex = /Rate\s+date:\s+(.*?)\s+sign:\s+(.*?)\s+value:\s+([\d.]+)/g;

    const ratesMap = {};

    let match;
    while ((match = rateRegex.exec(ratesData)) !== null) {
        const date = match[1];
        const sign = match[2];
        const value = parseFloat(match[3]);
        const key = `${sign}_${new Date(date).getTime()}`;
        ratesMap[key] = value;
    }

    return ratesMap
}

const parseAndSaveData = async ({dataToParse, ratesMap, transaction}) => {
    const employees = dataToParse.split('Employee').slice(1);

    for (let empData of employees) {
        const empLines = empData.split('\n').map(line => line.trim()).filter(line => line);

        const department = {
            id: parseInt(empLines[4].split(': ')[1]),
            name: empLines[5].split(': ')[1],
        }


        const employee = {
            id: parseInt(empLines[0].split(': ')[1]),
            name: empLines[1].split(': ')[1],
            surname: empLines[2].split(': ')[1],
            statements: [],
            donations: []
        };

        for (let i = 7; i < empLines.length; i++) {
            if (empLines[i].startsWith('Statement')) {
                const statement = {
                    id: parseInt(empLines[i + 1].split(': ')[1]),
                    amount: parseFloat(empLines[i + 2].split(': ')[1]),
                    date: new Date(empLines[i + 3].split(': ')[1]),
                };
                employee.statements.push(statement);
                i += 3;
            } else if (empLines[i].startsWith('Donation')) {
                const donationTime = new Date(empLines[i + 2].split(': ')[1])
                const donationAmount = empLines[i + 3].split(': ')[1]
                const [amount, currency] = donationAmount.trim().split(' ')

                const finalAmount = currency === 'USD' ? amount : amount * (ratesMap[`${currency}_${donationTime.getTime()}`] || 1)
                const donation = {
                    id: parseInt(empLines[i + 1].split(': ')[1]),
                    date: donationTime,
                    amount: finalAmount,
                };
                employee.donations.push(donation);
                i += 3;
            }
        }

        await Department.findOrCreate({
            where: { id: department.id },
            defaults: { name: department.name },
            transaction
        });

        await Employee.create(employee, {
            include: [
                {model: Statement, as: 'statements'},
                {model: Donation, as: 'donations'}
            ],
            transaction
        });
    }
};

const importDump = async () => {
    await sequelize.sync({force: true})

    const transaction = await sequelize.transaction();
    try {
        const filePath = path.join(__dirname, 'dump.txt');
        const data = fs.readFileSync(filePath, 'utf-8');
        const [dataToParse, ratesData] = data.split('Rates');

        const ratesMap = generateRatesMap(ratesData)
        await parseAndSaveData({dataToParse, ratesMap, transaction})

        await transaction.commit();
        console.log('Data imported successfully');
    } catch (error) {
        await transaction.rollback();
        console.error('Error importing data:', error);
    }
}

sequelize.sync({force: true}).then(async () => {
    await importDump()
})
