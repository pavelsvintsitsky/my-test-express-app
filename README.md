## Example of using the app

```bash
# docker
docker compose up 

# dependencies
npm install

# dump
npm run import

# run the api
npm run start
```

