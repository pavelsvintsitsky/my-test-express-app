const {Sequelize} = require('sequelize');

// TODO move variables to env
const sequelize = new Sequelize('postgres', 'pguser', 'superpass', {
    host: 'localhost',
    dialect: 'postgres',
});

module.exports = sequelize;
