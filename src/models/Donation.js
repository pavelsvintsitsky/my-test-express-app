module.exports = (sequelize, DataTypes) => {
    const Donation = sequelize.define(
        'Donation',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            date: DataTypes.DATEONLY,
            amount: DataTypes.DOUBLE,
            employeeId: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },
        {
            freezeTableName: true,
            tableName: 'donations',
            timestamps: false
        }
    );

    Donation.associate = (models) => {
        Donation.belongsTo(models.Employee, {
            foreignKey: 'employeeId',
            as: 'employee'
        });
    };

    return Donation;
};
