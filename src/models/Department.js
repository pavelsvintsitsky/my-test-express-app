module.exports = (sequelize, DataTypes) => {
    const Department = sequelize.define(
        'Department',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            name: DataTypes.STRING
        },
        {
            freezeTableName: true,
            tableName: 'departments',
            timestamps: false
        }
    );

    Department.associate = (models) => {
        Department.hasMany(models.Employee, {
            foreignKey: 'departmentId',
            as: 'employees'
        });
    };

    return Department;
};
