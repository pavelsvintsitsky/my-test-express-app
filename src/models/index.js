const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Employee = require('./Employee')(sequelize, DataTypes);
const Department = require('./Department')(sequelize, DataTypes);
const Statement = require('./Statement')(sequelize, DataTypes);
const Donation = require('./Donation')(sequelize, DataTypes);

const models = {
    Employee,
    Department,
    Statement,
    Donation
};

Object.keys(models).forEach((modelName) => {
    if (models[modelName].associate) {
        models[modelName].associate(models);
    }
});

module.exports = {
    ...models,
    sequelize
}
