module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define(
        'Employee',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            name: DataTypes.STRING,
            surname: DataTypes.STRING
        },
        {
            freezeTableName: true,
            tableName: 'employees',
            timestamps: false
        }
    );

    Employee.associate = (models) => {
        Employee.belongsTo(models.Department, {
            foreignKey: 'departmentId',
            as: 'department'
        });
        Employee.hasMany(models.Statement, {
            foreignKey: 'employeeId',
            as: 'statements'
        });
        Employee.hasMany(models.Donation, {
            foreignKey: 'employeeId',
            as: 'donations'
        });
    };

    return Employee;
};
