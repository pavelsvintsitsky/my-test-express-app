module.exports = (sequelize, DataTypes) => {
    const Statement = sequelize.define(
        'Statement',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            amount: DataTypes.DOUBLE,
            date: DataTypes.DATEONLY,
            employeeId: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },
        {
            freezeTableName: true,
            tableName: 'statements',
            timestamps: false
        }
    );

    Statement.associate = (models) => {
        Statement.belongsTo(models.Employee, {
            foreignKey: 'employeeId',
            as: 'employee'
        });
    };

    return Statement;
};
