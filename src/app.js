const express = require('express');
const bodyParser = require('body-parser');
const {sequelize} = require("./models");
const employeeRoutes = require('./routes/employeeRoutes');

const app = express();
const PORT = 3000;

app.use(bodyParser.json());
app.use('/api/v1', employeeRoutes);

sequelize.sync({ alter: true }).then(() => {
    app.listen(PORT, () => {
        console.log(`Server is running on port ${PORT}`);
    });
}).catch(error => {
    console.error('Unable to sync the database:', error);
});
