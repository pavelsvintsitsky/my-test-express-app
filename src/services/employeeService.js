const {sequelize} = require("../models");

const getRewardData = async () => {
    // TODO avoid string query usage
    const [results] = await sequelize.query(`
      SELECT 
        e.id,
        e.name,
        e.surname,
        SUM(d.amount::numeric) AS total_donations,
        CASE
          WHEN SUM(d.amount::numeric) > 100 THEN
            (SUM(d.amount::numeric) / (SELECT SUM(amount::numeric) FROM donations WHERE amount::numeric > 100)) * 10000
          ELSE 0
        END AS reward
      FROM employees e
      JOIN donations d ON e.id = d."employeeId"
      GROUP BY e.id, e.name, e.surname
      HAVING SUM(d.amount::numeric) > 100
    `);

    return results
}

module.exports = {
    getRewardData
};