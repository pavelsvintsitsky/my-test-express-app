const employeeService = require('../services/employeeService');

const getReward = async (req, res) => {
    try {
        const results = await employeeService.getRewardData();
        res.json(results);
    } catch (error) {
        console.error('Error fetching reward data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}

module.exports = {
    getReward
};