const express = require('express');
const employeeController = require('../controllers/employeeController');

const router = express.Router();

// TODO add auth middleware
router.get('/reward', employeeController.getReward);

module.exports = router;